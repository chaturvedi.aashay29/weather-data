import axios from "axios";
const baseURL = "";
const request = (method) => (options) => async (dispatch, getState) => {
  const t = Date.now();
  options.url = `${options.url}${options.url.indexOf("?") == -1 ? "?" : "&"}`;
  return axios({
    baseURL,
    ...options,
    method,
    proxy: false,
  }).catch((error) => {
    console.error("fetch error:", error);
  });
};

export const get = request("GET");
export const post = request("POST");
