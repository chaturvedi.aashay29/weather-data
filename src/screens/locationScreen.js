import React, { useEffect, useState } from "react";
import RNLocation from "react-native-location";
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  FlatList,
  StatusBar,
  Alert,
  Image,
  Button,
} from "react-native";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { getWeather, setLoaderFalse } from "../reduxFiles/actions";
import SplashyGif from "../../animation_splashy.gif";
import ErrorScreen from "./errorScreen";

const LocationScreen = () => {
  const dispatch = useDispatch();

  const weatherData = useSelector((state) => state.weather.weatherData);
  const showLoader = useSelector((state) => state.weather.loader);

  const [location, setLocation] = useState(undefined);
  const [weatherDataList, setWeatherDataList] = useState(undefined);
  const [currentDayData, setCurrentDayData] = useState(null);

  const getUserLocation = () => {
    RNLocation.getLatestLocation({ timeout: 1000 }).then((latestLocation) => {
      // Use the location here
      setLocation({
        latitude: latestLocation.latitude,
        longitude: latestLocation.longitude,
      });
    });
  };

  useEffect(() => {
    if (location != undefined) {
      RNLocation.configure({
        distanceFilter: 5.0,
      });

      RNLocation.requestPermission({
        ios: "whenInUse",
        android: {
          detail: "fine",
          rationale: {
            title: "Location permission",
            message: "We use your location to demo the library",
            buttonPositive: "OK",
            buttonNegative: "Cancel",
          },
        },
      }).then((granted) => {
        if (granted) {
          RNLocation.getLatestLocation({ timeout: 70000 }).then(
            (latestLocation) => {
              // Use the location here
              setLocation({
                latitude: latestLocation.latitude,
                longitude: latestLocation.longitude,
              });
            }
          );
        }
      });
      dispatch(getWeather(location.latitude, location.longitude));
    }
  }, [location]);

  useEffect(() => {
    setTimeout(() => {
      dispatch(setLoaderFalse());
    }, 3000);
  }, []);

  useEffect(() => {
    if (
      weatherData != null &&
      weatherData.list != undefined &&
      weatherData.list.length > 0
    ) {
      parseWeatherData(weatherData);
    }
  }, [weatherData]);

  const parseWeatherData = (dataOfWeather) => {
    let daysInStrings = dataOfWeather.list
      .filter((item) => {
        return item.dt_txt.includes("12:00:00");
      })
      .map((item) => {
        return {
          date: moment(item.dt_txt),
          temperature: item.main.temp - 273,
        };
      })
      .map((item) => {
        return {
          day: moment(item.date).format("dddd"),
          temperature: item.temperature.toFixed(2),
        };
      });

    setCurrentDayData(daysInStrings.shift());
    setWeatherDataList(daysInStrings);
  };

  const Item = ({ itemObj }) => (
    <View style={styles.item}>
      <Text>{itemObj.day}</Text>
      <Text style={{ marginLeft: "auto" }}>{itemObj.temperature}</Text>
    </View>
  );

  const renderItem = ({ item }) => (
    <View
      style={{
        margin: 5,
        borderRadius: 10,
        borderWidth: 2,
      }}
    >
      <Item itemObj={item} />
    </View>
  );

  return (
    <SafeAreaView style={styles.innerContainer}>
      {location && Object.keys(weatherData).length > 0 && !showLoader ? (
        <React.Fragment>
          <View
            style={{
              marginTop: 50,
              alignItems: "center",
            }}
          >
            {currentDayData && (
              <Text style={styles.valueTitle}>
                {currentDayData.temperature}
              </Text>
            )}
            <Text style={styles.valueTitle}>{weatherData.city.name}</Text>
          </View>

          <View
            style={{
              width: "100%",
              marginTop: "auto",
            }}
          >
            <FlatList
              data={weatherDataList}
              renderItem={renderItem}
              keyExtractor={(item) => item.day}
            />
          </View>
        </React.Fragment>
      ) : showLoader ? (
        <View style={styles.centerScreen}>
          <Image source={SplashyGif} style={styles.tinyLogo} />
        </View>
      ) : (
        <View style={styles.centerScreen}>
          <ErrorScreen getUserLocation={() => getUserLocation()} />
        </View>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  innerContainer: {
    flex: 1,
    backgroundColor: "white",
    paddingVertical: 30,
  },
  valueTitle: {
    fontFamily: "Futura",
    fontSize: 35,
    fontWeight: "bold",
  },
  tinyLogo: {
    width: 100,
    height: 100,
  },
  centerScreen: {
    display: "flex",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  item: {
    flexDirection: "row",
    alignItems: "center",
    padding: 10,
  },
});

export default LocationScreen;
