import React from "react";
import { Text, Button } from "react-native";

const ErrorScreen = (props) => {
  const { getUserLocation } = props;

  return (
    <React.Fragment>
      <Text>Couldn't fetch Data. Please try again later.</Text>
      <Button title="Try Again" onPress={() => getUserLocation()} />
    </React.Fragment>
  );
};

export default ErrorScreen;
