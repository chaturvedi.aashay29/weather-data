import * as actionConstants from "../../utils/actionConstants";

const INITIAL_STATE = {
  weatherData: {},
  loader: true,
};

export const WeatherReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actionConstants.WEATHER_DATA:
      return {
        ...state,
        weatherData: action.payload,
        loader: false,
      };
    case actionConstants.LOADER_FALSE:
      return {
        ...state,
        loader: false,
      };
    default:
      return state;
  }
};
