import { combineReducers } from "redux";
import { WeatherReducer as weather } from "./weatherReducer";

const appReducers = combineReducers({
  weather,
});

const rootReducer = (state, action) => {
  return appReducers(state, action);
};

export default rootReducer;
