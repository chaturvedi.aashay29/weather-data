import * as actionConstants from "../../utils/actionConstants";
import { api } from "../../utils/apiConstants";
import { get, post } from "../../utils/fetch";

export const getWeather = (lat, lon) => (dispatch, getState) => {
  get({
    url:
      api.getWeatherFromApi +
      "lat=" +
      lat +
      "&lon=" +
      lon +
      "&appid=90a5e9473c5e352469dc155396ef6852",
  })(dispatch, getState)
    .then((response) => {
      if (response != undefined) {
        dispatch({
          type: actionConstants.WEATHER_DATA,
          payload: response.data,
        });
      }
    })
    .catch((error) => {
      console.log("error = ", error);
    });
};

export const setLoaderFalse = () => (dispatch, getState) => {
  dispatch({
    type: actionConstants.LOADER_FALSE,
    payload: "loader_false",
  });
};
